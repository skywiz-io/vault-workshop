# Solution - Walkthrough

## Step 1: Login to vault UI

1. Open a web browser and launch the Vault UI (e.g. http://< Instane External 
   IP >:8200/ui).

2. On method choose token (token is the default login method).

3. Paste the root token.

4. Click on sign in.

  ![](img/login.png)

## Step 2: Create a policy

To perform all tasks demonstrated in this exercise, your policy must include the following permissions:

```
# Write and manage secrets in key-value secrets engine
path "secret*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# To enable secrets engines
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete" ]
}
```

Navigate to Policies

  ![](img/policies.png)

And than click on create policy

  ![](img/create_policy.png)

At the create ACL policy page, give a name to the policy and copy than paste the policy from above.

  ![](img/ACL.png)

To update to policy click on Create Policy

## Step 3: Write secrets

To understand how the versioning works, let's write some test data.

1. In the Web UI, select secret/ and then click Create secret. Enter the 
   secrets. You can click on the sensitive information toggle to show or hide the entered secret values.

  ![](img/create_secret.png)
  
2. Click Save.

3. To update the existing secret, select Create new version, change the 
   contact_email value, and then click Save.

  ![](img/create_new_version.png)
  
## Step 4: Retrieve a specific version of secret

You may run into a situation where you need to view the secret before an update.

1. Select History and then Version 1

  ![](img/history.png)

2. The version changes from Version 2 to Version 1. To view the raw values, 
   click on the sensitive information toggle.

  ![](img/version1.png)

3. You can create a new version of the secrets based on an older version.  
   Select History > View version history.
   Select the version you wish to based the data on, and then select Create new version from 1.

  ![](img/create_new_version_from_1.png)
   
This can help revert an older version of the secrets.

## Step 5: Specify the number of versions to keep

By default, the kv-v2 secrets engine keeps up to 10 versions. Let's limit the maximum number of versions to keep to be 4.

1. Click the Vault CLI shell icon (>_) to open a command shell. Execute the 
   following command to tune the maximum number of 
   versions to keep: vault write secret/config max_versions=4

  ![](img/max_versions.png)
   
2. Click the icon (>_) again to hide the shell.

3. Overwrite the data a few more times to see what happens to its versions.

  ![](img/versions_history.png)

In this example, the current version is 6. Notice that version 1 and 2 do not show up in the metadata. Because the kv secrets engine is configured to keep only 4 versions, the oldest two versions are permanently deleted and you won't be able to read them.

## Step 6: Delete versions of secret

Let's delete versions 4 and 5.

1. At the secret/customer/acme path, select History > View version history. 
   Expand the menu for Version 5 and
   select Delete version.

  ![](img/delete_version.png)
  
2. At the confirmation dialog, click Delete to proceed. Repeat the steps for 
   Version 4 Now.
   Version 4 and 5 should be marked as deleted.

3. If version 5 was deleted by mistake and you wish to recover, select Undelete
   version from its menu.

## Step 7: Permanently delete data

To permanently delete a version of secret, simply select Permanently destroy version from its menu.

  ![](img/permanently_destroy.png)