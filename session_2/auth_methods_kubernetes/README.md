# Integrate a Kubernetes Cluster (EKS) with an External Vault

## Overview

One of the most common and recommended approach for storing secrets in a secure manner, is to use a secure secrets store such as Vault, coupled with Kubernetes integration.
We are already using a vault server to store the secrets, but wanted to improve upon the current approach in the best manner possible.
We need a solution, especially for secrets that are dynamically getting updated, to securely integrate secrets without a deployment. Hashicorp provides a vault-agent as a sidecar solution which fits into this requirement.

The Vault Agent Injector alters pod specifications to include Vault Agent containers that render Vault secrets to a shared memory volume using Vault Agent Template. By rendering secrets to a shared volume, containers within a pod can consume Vault secrets without being Vault aware.

The injector is a Kubernetes Mutation Webhook Controller. The controller intercepts pod events and applies mutations to the pod if a particular annotation (“vault.hashicorp.com/agent-inject: "true"”) exists within the request. This functionality is provided by the vault-k8s project and can be automatically installed and configured using the vault-helm chart.

Since we already have a vault that is EC2 based and did not want to migrate to Kubernetes as of now. Our idea is to use our existing vault server and integrate a vault agent container with a vault running on EC2.

In this lab, we will create a Kubernetes service account; configure Vault's Kubernetes authentication and create a role to access a secret; install the Vault Helm chart to run only the injector service; and patch a deployment.

  ![](img/k8s-vault.jpg)

## Implementation Walk Through

The complete solution can be implemented in four steps detailed below:

  1. Create a kubernetes namespace and config kubectl to use it by default
  2. Vault agent integration with external vault server
  3. Enabling Kubernetes vault authentication
  4. Injecting secrets into application

## Step 1: Create a kubernetes namespace and config kubectl to use it by default

Create a kubernetes namespace

```
kubectl create namespace vault-<My Name>
```

Config kubectl to use it by default

```
kubectl config set-context --current --namespace=vault-<My Name>
```

Add label

```
kubectl label namespaces vault-<My Name> owner=<My Name>
```

## Step 2: Vault agent integration with external vault server

We only need to configure vault-agent with our vault server and don’t need to install the complete helm chart of vault:

```
helm repo add hashicorp https://helm.releases.hashicorp.com
```

**Please change the Name and the IP address according to your personal settings**

```
helm install vault hashicorp/vault \
    --set "injector.externalVaultAddr=http://<Instance External IP>:8200" \
    --set "injector.namespaceSelector.matchLabels.owner=<My Name>"
```

The Vault Agent Injector pod is deployed in the given namespace.

```
NAME                                        READY   STATUS    RESTARTS   AGE
vault-agent-injector-7b6cd469d8-8svg5       1/1     Running   0          15s
```

Wait until the vault-agent-injector pod reports that it is running and ready (1/1).

Vault agent injector works at pod create and update events of Kubernetes. Kubernetes controller looks for pod annotation vault.hashicorp.com/agent-inject: true and if finds it, it will alter pod with other annotations available for the pod.

Since vault-agent is a webhook which works with Kubernetes mutation webhook controller, at minimum Hashicorp vault has provided volume mounted at /vault/secrets and will be used by the Vault Agent containers for sharing secrets with the other containers in the pod.

## Step 3: Enable Kubernetes Vault Authentication

Vault provides authentication via a Service account. Each microservice/application configured with their own service account to avoid accessing secrets with a single service account. But first, we needed to configure Kubernetes vault authentication. Vault provided a Kubernetes authentication method to enable vault communication with Kubernetes using a service account.

Enabling Kubernetes vault authentication

```
vault auth enable kubernetes
```

A service creates an abstraction around pods or an external service. When an application running in a pod requests the service, that request is routed to the endpoints that share the service name.

Deploy a service named external-vault and a corresponding endpoint configured to address the EXTERNAL_VAULT_ADDR.

```
cat <<EOF | kubectl apply -f -
---
apiVersion: v1
kind: Service
metadata:
  name: external-vault
spec:
  ports:
  - protocol: TCP
    port: 8200
---
apiVersion: v1
kind: Endpoints
metadata:
  name: external-vault
subsets:
  - addresses:
      - ip: $(curl http://api.ipify.org/)
    ports:
      - port: 8200
EOF
```

Create a service account vault_auth and enable RBAC using clusterRoleBinding to grant access cluster-wide

**Change the namespace and the metadata name to your own**

``` yaml
cat <<EOF | kubectl create -f -
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: vault-auth
---
apiVersion: v1
kind: Secret
metadata:
  name: vault-auth
  annotations:
    kubernetes.io/service-account.name: vault-auth
type: kubernetes.io/service-account-token
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: role-tokenreview-binding-<My Name>
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
  - kind: ServiceAccount
    name: vault-auth
    namespace: vault-<My Name>
EOF
```

Change < My Name > with your name.

The output should be:

``` bash
serviceaccount/vault-auth created
secret/vault-auth created
clusterrolebinding.rbac.authorization.k8s.io/role-tokenreview-binding created
```

Vault accepts this service token from any client within the Kubernetes cluster. During authentication, Vault verifies that the service account token is valid by querying a configured Kubernetes endpoint. To configure it correctly requires capturing the JSON web token (JWT) for the service account, the Kubernetes CA certificate, and the Kubernetes host URL.

First, get the JSON web token (JWT) for this service account.

```
TOKEN_REVIEW_JWT=$(kubectl get secret vault-auth -o go-template='{{ .data.token }}' | base64 --decode)
```

Next, retrieve the Kubernetes CA certificate.

```
KUBE_CA_CERT=$(kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.certificate-authority-data}' | base64 --decode)
```

Next, retrieve the Kubernetes host URL.

```
KUBE_HOST=$(kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.server}')
```

Finally, configure the Kubernetes authentication method to use the service account token, the location of the Kubernetes host, and its certificate.

```
vault write auth/kubernetes/config \
        token_reviewer_jwt="$TOKEN_REVIEW_JWT" \
        kubernetes_host="$KUBE_HOST" \
        kubernetes_ca_cert="$KUBE_CA_CERT"
```

The output

```
Success! Data written to: auth/kubernetes/config
```

## Step 4: Injecting secrets into application

Now we need that application should be able to read the vault secrets using a vault-agent as a sidecar container. Hashicorp vault provides two methods to use vault-agent integration as sidecar to read secrets:

  1. the vault.hashicorp.com/agent-inject-secret annotation
  2. a configuration map containing Vault Agent configuration files.
  - (Note: Only one of these methods can be used at a time).
We have configured our application to access secrets using vault.hashicorp.com/agent-inject-secret annotation.

Export the VAULT_ADDR environment variable

```
export VAULT_ADDR=http://0.0.0.0:8200
```

Login with the root token

```
vault login $(cat .vault-token)
```

Put secrets into secret/ path

```
vault kv put secret/myapp username='giraffe' password='salsa'
```

Insure we able to put the secret

```
vault kv get secret/myapp
```

The output

```
====== Metadata ======
Key              Value
---              -----
created_time     2020-12-13T18:00:34.413359655Z
deletion_time    n/a
destroyed        false
version          1

====== Data ======
Key         Value
---         -----
password    salsa
username    giraffe
```

Create vault policy to read secrets from defined path

```
vault policy write app-read-policy - <<EOF
path "secret/data/myapp" {
  capabilities = ["read"]
}
EOF
```

The output

```
Success! Uploaded policy: app-read-policy
```

And then create service account in desired namespace

```
kubectl create serviceaccount myapp-sa
```

The output

```
serviceaccount/myapp-sa created
```

Last vault role to bound service account and policy for authentication.
**Please change the namespace**

```
vault write auth/kubernetes/role/myapp-role \
        bound_service_account_names=myapp-sa \
        bound_service_account_namespaces=vault-<My Name> \
        policies=app-read-policy \
        ttl=24h
```

The output

```
Success! Data written to: auth/kubernetes/role/myapp-role
```

Create deployment manifest

```
cat <<EOF | kubectl apply -f -
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp
  labels:
    app: vault-agent-injector
spec:
  replicas: 1
  selector:
    matchLabels:
      app: vault-agent-injector
  template:
    metadata:
      labels:
        app: vault-agent-injector
    spec:
      serviceAccountName: myapp-sa
      containers:
      - name: myapp
        image: nginx
EOF
```

The output

```
deployment.apps/myapp created
```

Get list of pods

```
kubectl get pods
```

An example output

```
NAME                                   READY   STATUS    RESTARTS   AGE
myapp-6cd8c7547-tfw4w                  1/1     Running   0          10s
vault-agent-injector-85785f74c-vxcnz   1/1     Running   0          2m54s
```

Let’s apply the vault annotation and apply the same to our running application myapp

Copy the template

```
---
spec:
  template:
    metadata:
      annotations:
        vault.hashicorp.com/agent-inject: "true"
        vault.hashicorp.com/role: "myapp-role"
        vault.hashicorp.com/agent-inject-secret-myapp.txt: "secret/myapp"
        vault.hashicorp.com/agent-inject-template-myapp.txt: |
          {{ with secret "kv/data/secret/myapp" -}}
          {{ range $k, $v := .Data.data }}
          export {{ $k }}={{ $v }}
          {{ end }}
          {{- end }}
```

And paste it to a file myapp-vault-agent-patch.yml

```
sudo vi myapp-vault-agent-patch.yml
```

Now patch the deployment with the template

```
kubectl patch deployments.apps myapp --patch "$(cat myapp-vault-agent-patch.yml)"
```

Wait until the re-deployed myapp pod reports that it is running and ready (2/2).

The output

```
NAME                                   READY   STATUS    RESTARTS   AGE
myapp-69c8fcf95-hktz2                  2/2     Running   0          27m
vault-agent-injector-85785f74c-ctm5p   1/1     Running   0          32m
```

Let’s run the command to get the content from /vault/secrets path:

```
kubectl exec < The name of the new myapp pod > --container myapp -- cat /vault/secrets/myapp.txt
```

The output:

```
data: map[password:salsa username:giraffe]
metadata: map[created_time:2020-12-13T18:00:34.413359655Z deletion_time: destroyed:false version:1]
```

So as we can see the secrets are available in the application pod in the shared volume mount, the secrets can be templated based on the application need.
