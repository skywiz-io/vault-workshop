# Initializing and Unsealing the Vault

## Overview

The operator init command initializes a Vault server. Initialization is the process by which Vault's storage backend is prepared to receive data. Since Vault servers share the same storage backend in HA mode, you only need to initialize one Vault to initialize the storage backend.

During initialization, Vault generates an in-memory master key and applies Shamir's secret sharing algorithm to disassemble that master key into a configuration number of key shares such that a configurable subset of those key shares must come together to regenerate the master key. These keys are often called "unseal keys" in Vault's documentation.

This command cannot be run against already-initialized Vault cluster.

## Init

Vault by deafult set to work with https, for security reasons,
Export an environment variable for the vault CLI to address the Vault server

```
export VAULT_ADDR=http://0.0.0.0:8200
```

Start initialization with the default options

```
vault operator init
```

Initialization outputs two incredibly important pieces of information: the unseal keys and the initial root token. This is the only time ever that all of this data is known by Vault, and also the only time that the unseal keys should ever be so close together.

For the purpose of this lab, save all of these keys somewhere, and continue. In a real deployment scenario, you would never save these keys together.
Vault initialized with 5 key shares and a key threshold of 3 by default.

It's possible to define the key shares and the key threshold instead of the default numbers

```
vault operator init \
    -key-shares=3 \
    -key-threshold=2
```

## Unseal

Every initialized Vault server starts in the sealed state. From the configuration, Vault can access the physical storage, but it can't read any of it because it doesn't know how to decrypt it. The process of teaching Vault how to decrypt the data is known as unsealing the Vault.
Unsealing has to happen every time Vault starts.

unsealing the Vault:

```
vault operator unseal
```

The output should be:

```
Unseal Key (will be hidden):
...
```

Paste one of the unseal keys from the init step and press ENTER,
Continue with vault operator unseal to complete unsealing the Vault. To unseal the vault you must use three different unseal keys, the same key repeated will not work.
After pasting in a valid key and confirming, you see that Vault is still sealed, but progress is made. Vault knows it has 1 key out of 3. Due to the nature of the algorithm, Vault doesn't know if it has the correct key until the threshold is reached.

```
$ vault operator unseal

Unseal Key (will be hidden):
Key                Value
---                -----
Seal Type          shamir
Initialized        true
Sealed             true
Total Shares       5
Threshold          3
Unseal Progress    1/3
Unseal Nonce       d3d06528-aafd-c63d-a93c-e63ddb34b2a9
Version            1.3.4
HA Enabled         true
```

Also notice that the unseal process is stateful. You can go to another computer, use vault operator unseal, and as long as it's pointing to the same server, that other computer can continue the unseal process. This is incredibly important to the design of the unseal process: multiple people with multiple keys are required to unseal the Vault. The Vault can be unsealed from multiple computers and the keys should never be together. A single malicious operator does not have enough keys to be malicious.

As you use keys, as long as they are correct, you should soon see output like this:

```
Key                     Value
---                     -----
Seal Type               shamir
Initialized             true
Sealed                  false
Total Shares            5
Threshold               3
Version                 1.5.0
Cluster Name            vault-cluster-0ba62cae
Cluster ID              7d49e5fd-a1a4-c1d1-55e2-7962e43006a1
HA Enabled              true
HA Cluster              n/a
HA Mode                 standby
Active Node Address     <none>
Raft Committed Index    24
Raft Applied Index      24
```

When the value for Sealed changes to false, the Vault is unsealed.

Finally, authenticate as the initial root token (it was included in the output with the unseal keys):

```
vault login <Root Token>
```

The output should be:

```

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.spAZOi7SlpdFTNed50sYYCIU
token_accessor       OevFmMXjbmOCQ8rSubY84vVp
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```

As a root user, you can reseal the Vault with vault operator seal. A single operator is allowed to do this. This lets a single operator lock down the Vault in an emergency without consulting other operators.

When the Vault is sealed again, it clears all of its state (including the encryption key) from memory. The Vault is secure and locked down from access.

## vault UI

At this point to vault UI should look:

  ![](img/vault-ui-2.png)
