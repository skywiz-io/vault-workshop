# Audit Devices

## Overview

Audit devices are the components in Vault that keep a detailed log of all requests and response to Vault. Because every operation with Vault is an API request/response, the audit log contains every authenticated interaction with Vault, including errors.

Multiple audit devices can be enabled and Vault will send the audit logs to all of them. This allows you to not only have a redundant copy, but also a second copy in case the first is tampered with.

There are 3 Audit devices which can be enabled File, Syslog and Socket.

If you have more than one audit device, then Vault will complete the request as long as one audit device persists the log.

Vault will not respond to requests if audit devices are blocked because audit logs are critically important and ignoring blocked requests opens an avenue for attack. Be absolutely certain that your audit devices cannot block.

## Format

Each line in the audit log is a JSON object. The type field specifies what type of object it is. Currently, only two types exist: request and response. The line contains all of the information for any given request and response. By default, all the sensitive information is first hashed before logging in the audit logs.

## Audit Device - File

The file audit device writes audit logs to a file. This is a very simple audit device: it appends logs to a file.

Create a file for the logs on /var/log/vault_audit.log path.

```
sudo touch /var/log/vault_audit.log
```

Change the owner of the file to vault user:

```
sudo chown -R vault:vault /var/log/vault_audit.log
```

Enable at the default path:

```
vault audit enable file file_path=/var/log/vault_audit.log
```

It's possible to enable at a different path. It is possible to enable multiple copies of an audit device:

```
vault audit enable -path="vault_audit_1" file file_path=/home/user/vault_audit.log
```

And also to enable logs on stdout. This is useful when running in a container:

```
vault audit enable file file_path=stdout
```