# Vault Workshop

Lab to practice hashicorp vault.
Vault is a tool to secure, store and tightly control access to tokens, passwords, certificates, encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API.

## Prerequisites and installations
1. **Connect to AWS environment** - [AWS Environment](session_1/aws_environment)
2. **Prerequisites** - [Prerequisites](session_1/prerequisites)
3. **Hashicorp consul installation** - [Consul](session_1/consul_installation)
4. **Hashicorp vault installation** - [Vault](session_1/vault_installation)
5. **Intruducing to consul and vault UI** - [UI](session_1/UI)

## Vault
1. **Initialize and Unseal Vault** - [Initialize and Unseal](session_2/init_unseal) 
2. **Policies** - [Policies](session_2/policies)
3. **Secret Engine KV version 2** - [KV2](session_2/kv2)
4. **Exercise: Policies and KV version 2 from the UI** - [Exercise](session_2/exercise)
5. **Solution: Walkthrough** - [Solution](session_2/solution)
6. **Secret Engine: MYSQL Database (dynamic credentials)** - [Dynamic Credentials](session_2/dynamic_credentials)
7. **Auth Methods: Kubernetes** - [Auth Methods - Kubernetes](session_2/auth_methods_kubernetes)
8. **Audit Devices** - [Audit Devices - File](session_2/audit_devices)