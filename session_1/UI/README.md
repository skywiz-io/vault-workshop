# Consul UI

## Overview

Consul features a web-based user interface, allowing you to easily view all services, nodes, intentions and more using a graphical user interface, rather than the CLI or API.

## Connect to the UI

Launch a web browser, and enter http://< Instance External IP >:8500 in the address

## Explore the UI

  ![](img/consul-ui.png)

As you can see consul service is up and running,
But there is an error on the vault service,
If we navigate to Nodes, we can see the health check of the vault,

  ![](img/health-check.png)

The reason for this error is:
Vault service is healthy when Vault is in an unsealed status and can become an active Vault server

# Vault UI

## Overview

Vault features a web-based user interface (UI) that enables you to unseal, authenticate, manage policies and secrets engines

## Connect to the UI

Launch a web browser, and enter http://< Instance External IP >:8200/ui in the address

## Explore the UI

  ![](img/vault-ui.png) 

At this point the vault server is uninitialized and sealed